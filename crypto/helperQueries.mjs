import { encodingOptions, hashEncodingOptions } from '../util/encoding-options.mjs';
import { hashingOptions } from '../util/hashing-options.mjs';
import { EncodingOptionError, HashOptionError, MinimumKeyLengthError } from '../util/error.mjs';

/**
 * @typedef {object} CreateSaltAndPasswordHashOptions
 * @prop {number} keyLength The length of the value (default: 16)
 * @prop {number} iterations The number of function iterations to run which helps prevent brute force password cracking (default: 500)
 * @prop {import('../util/hashing-options.mjs').HashingOption} algorithm The algorithm used to generate the hash (default: sha512)
 * @prop {import('../util/encoding-options.mjs').HashEncodingOption} encoding The encoding used to present the hash and store it (default: hex)
 */

/**
 *
 * @param {import('better-sqlite3').Database} db
 * @returns
 */
function createSaltAndPasswordHash(db) {
  const DEFAULTS = Object.freeze({
    algorithm: 'sha512',
    encoding: 'hex',
    iterations: 500,
    keyLength: 16
  });
  /**
   * Create a helper sql statement that takes an object containing one key, password and returns
   * a newly generate salt value and the resulting hash of the provided password using the salt
   * value
   * @param {CreateSaltAndPasswordHashOptions} options
   * @returns {import('better-sqlite3').Statement<{password: string}, {salt: string, hash: string}>} Sql statement for generating password hashes and salt values
   */
  return function createSaltAndPasswordHash(options = {}) {
    if (!options.algorithm) options.algorithm = DEFAULTS.algorithm;
    if (!options.encoding) options.encoding = DEFAULTS.encoding;
    if (!options.iterations) options.iterations = DEFAULTS.iterations;
    if (!options.keyLength) options.keyLength = DEFAULTS.keyLength;
    if (!hashEncodingOptions.includes(options.encoding))
      throw new EncodingOptionError(options.encoding, true);
    if (!hashingOptions.includes(options.algorithm)) throw new HashOptionError(options.algorithm);
    if (options.keyLength < 16) throw new MinimumKeyLengthError(options.keyLength);
    const sql = db.prepare(`
     WITH sq AS (
       SELECT genSalt(${options.keyLength}) AS salt
     ),
     hq AS (
       SELECT sq.salt as salt,
       genPasswordHash(
         @password,
         sq.salt,
         ${options.keyLength},
         ${options.iterations},
         '${options.algorithm}',
         '${options.encoding}'
       ) AS hash
       FROM sq
     )
     SELECT hq.* FROM hq`);
    return sql;
  };
}

export default function (db) {
  return {
    createSaltAndPasswordHash: createSaltAndPasswordHash(db)
  };
}
