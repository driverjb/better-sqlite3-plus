import crypto from 'crypto';
import { hashingOptions } from '../util/hashing-options.mjs';
import { hashEncodingOptions } from '../util/encoding-options.mjs';
import { HashOptionError, EncodingOptionError } from '../util/error.mjs';

/**
 * Create a hash of the given data
 * @param {string} data
 * @param {import('../util/hashing-options.mjs').HashingOption} algorithm
 * @param {import('../util/encoding-options.mjs').HashEncodingOption} format
 * @returns
 */
function genHash(data, algorithm, encoding) {
  if (!hashingOptions.includes(algorithm)) throw new HashOptionError(algorithm);
  if (!hashEncodingOptions.includes(encoding)) throw new EncodingOptionError(encoding, true);
  return crypto.createHash(algorithm).update(data).digest(encoding);
}

/**
 *
 * @param {string} data
 * @param {import('../util/hashing-options.mjs')} algorithm
 */
function genHash2(data, algorithm) {
  return genHash(data, algorithm, 'hex');
}

/**
 *
 * @param {string} data
 * @param {import('../util/hashing-options.mjs')} algorithm
 */
function genHash3(data) {
  return genHash(data, 'sha256', 'hex');
}

/**
 * Add hashing functions to better sqlite3 instance
 * @param {import('better-sqlite3').Database} db
 */
export default function (db) {
  db.function('genHash', genHash);
  db.function('genHash', genHash2);
  db.function('genHash', genHash3);
}
