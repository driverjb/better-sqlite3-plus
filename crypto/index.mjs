import genSalt from './genSalt.mjs';
import genHash from './genHash.mjs';
import genPasswordHash from './genPasswordHash.mjs';
import helperQueries from './helperQueries.mjs';

/**
 * Add cryptography related functions to a better-sqlite3 instance and return helper queries
 * @param {import('better-sqlite3').Database} db
 * @returns
 */
export default function (db) {
  genSalt(db);
  genHash(db);
  genPasswordHash(db);
  return helperQueries(db);
}
