import crypto from 'crypto';
import { hashingOptions } from '../util/hashing-options.mjs';
import { encodingOptions } from '../util/encoding-options.mjs';
import { HashOptionError, EncodingOptionError } from '../util/error.mjs';

const DEFAULT_ENCODING = 'hex';
const DEFAULT_ALGORITHM = 'sha512';
const DEFAULT_KEY_LENGTH = 32;
const DEFAULT_ITERATIONS = 500;

/**
 *
 * @param {string} password
 * @param {string} salt
 * @param {number} iterations
 * @param {number} keyLength
 * @param {import('../util/hashing-options.mjs').HashingOption} algorithm
 * @param {import('../util/encoding-options.mjs').EncodingOption} encoding
 */
function genPasswordHash(password, salt, keyLength, iterations, algorithm, encoding) {
  if (!hashingOptions.includes(algorithm)) throw new HashOptionError(algorithm);
  if (!encodingOptions.includes(encoding)) throw new EncodingOptionError(encoding, false);
  return crypto.pbkdf2Sync(password, salt, iterations, keyLength, algorithm).toString(encoding);
}

function genPasswordHash2(password, salt, keyLength, iterations, algorithm) {
  return genPasswordHash(password, salt, iterations, keyLength, algorithm, DEFAULT_ENCODING);
}

function genPasswordHash3(password, salt, keyLength, iterations) {
  return genPasswordHash(
    password,
    salt,
    keyLength,
    iterations,
    DEFAULT_ALGORITHM,
    DEFAULT_ENCODING
  );
}

function genPasswordHash4(password, salt, keyLength) {
  return genPasswordHash(
    password,
    salt,
    keyLength,
    DEFAULT_ITERATIONS,
    DEFAULT_ALGORITHM,
    DEFAULT_ENCODING
  );
}

function genPasswordHash5(password, salt) {
  return genPasswordHash(
    password,
    salt,
    DEFAULT_KEY_LENGTH,
    DEFAULT_ITERATIONS,
    DEFAULT_ALGORITHM,
    DEFAULT_ENCODING
  );
}

/**
 * Add hashing functions to better sqlite3 instance
 * @param {import('better-sqlite3').Database} db
 */
export default function (db) {
  db.function('genPasswordHash', genPasswordHash);
  db.function('genPasswordHash', genPasswordHash2);
  db.function('genPasswordHash', genPasswordHash3);
  db.function('genPasswordHash', genPasswordHash4);
  db.function('genPasswordHash', genPasswordHash5);
}
