import crypto from 'crypto';
import { encodingOptions } from '../util/encoding-options.mjs';
import { EncodingOptionError } from '../util/error.mjs';

/**
 * Generate a salt value of the given size and encoding
 * @param {number} size
 * @param {import('./util/encoding-options.mjs').EncodingOption} encoding
 * @returns {string}
 */
function genSalt(size, encoding) {
  if (!encodingOptions.includes(encoding)) throw new EncodingOptionError(encoding);
  return crypto.randomBytes(size).toString(encoding);
}

/**
 * Generate a salt value of the given size
 * @param {number} size
 * @param {import('./util/encoding-options.mjs').EncodingOption} encoding
 * @returns {string}
 */
function genSalt2(size) {
  return genSalt(size, 'hex');
}

/**
 * Generate a salt value of 32 bits and hex encoding
 * @param {number} size
 * @param {import('./util/encoding-options.mjs').EncodingOption} encoding
 * @returns {string}
 */
function genSalt3() {
  return genSalt(32, 'hex');
}

/**
 * Add cryptography related functions to a better-sqlite3 instance
 * @param {import('better-sqlite3').Database} db
 */
export default function (db) {
  db.function(`genSalt`, genSalt);
  db.function(`genSalt`, genSalt2);
  db.function(`genSalt`, genSalt3);
}
