/** @typedef {'md5'|'sha256'|'sha512'|'sha1'|'sha384'|'sha224'} HashingOption */

export const hashingOptions = ['md5', 'sha256', 'sha512', 'sha1', 'sha384', 'sha224'];
