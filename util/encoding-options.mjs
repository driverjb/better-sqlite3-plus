/**
 * @typedef {'ascii'|'base64'|'base64url'|'binary'|'hex'|'latin1'|'ucs-2'|'ucs2'|'utf-16le'|'utf-8'|'utf16le'|'utf8'} EncodingOption
 */

export const encodingOptions = [
  'ascii',
  'base64',
  'base64url',
  'binary',
  'hex',
  'latin1',
  'ucs-2',
  'ucs2',
  'utf-16le',
  'utf-8',
  'utf16le',
  'utf8'
];

/** @typedef {'base64'|'base64url'|'binary'|'hex'} HashEncodingOption */

export const hashEncodingOptions = ['base64', 'base64url', 'binary', 'hex'];
