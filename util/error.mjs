import { encodingOptions, hashEncodingOptions } from './encoding-options.mjs';
import { hashingOptions } from './hashing-options.mjs';

function generateName(name) {
  return `BetterSqlite3Plus:${name}`;
}

export class EncodingOptionError extends Error {
  /**
   *
   * @param {string} invalidOption
   * @param {boolean} limited
   */
  constructor(invalidOption, limited = false) {
    const options = limited ? encodingOptions.join(', ') : hashEncodingOptions.join(', ');
    const message = `Invalid encoding option provided: ${invalidOption}. Use one of: ${options}`;
    super(message);
    this.name = generateName('EncodingOptionError');
  }
}

export class HashOptionError extends Error {
  /**
   *
   * @param {string} invalidOption
   */
  constructor(invalidOption) {
    const options = hashingOptions.join(', ');
    const message = `Invalid hash option provided: ${invalidOption}. Use one of: ${options}`;
    super(message);
    this.name = generateName('HashOptionError');
  }
}

export class MinimumKeyLengthError extends Error {
  constructor(invalidKeyLength) {
    const message = `A minimum key length of 16 bytes is required for safety. Requested length was ${invalidKeyLength}`;
    super(message);
    this.name = generateName('MinimumKeyLengthError');
  }
}
