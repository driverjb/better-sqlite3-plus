# better-sqlite3-plus

This module is a collection of functions that can be added to your sqlite3 database instance. The
aim is to provide common database functionality that will speed up development of features commonly
found a database.

# crypto

## Functions

A number of functions will be added to your sqlite3 instance. Those functions are as follows:

### genHash

Generates a hash value based on the provided string input

#### Parameters

- **data** (Required): A string representing the value to be hashed
- **algorithm** (Default: sha256): The hashing algorithim to use
- **encoding** (Default: hex): The encoding to be used when generating the output

```javascript
import Database from 'better-sqlite3';
import bs3plus from 'better-sqlite3-plus';

const db = new Database('test.db');

bs3plus.crypto(db);

const { hash } = db
  .prepare(`select genHash(?, ?, ?) as hash`)
  .get('string to hash', 'sha256', 'hex');

const { hash } = db.prepare(`select genHash(?, ?) as hash`).get('string to hash', 'sha256');

const { hash } = db.prepare(`select genHash(?) as hash`).get('string to hash');
```

### genPasswordHash

Generates a password hash value based on the provided inputs

#### Parameters

- **password** (Required): The password that will be used to generate the hash
- **salt** (Required): The salt value used to enhance the security of the password
- **keyLength** (Default: 32): The length of the generated hash value
- **iterations** (Default: 500): Number of iterations to use when generating the hash
- **algorithm** (Default: sha512): The algorithm to use when generating the has value
- **encoding** (Default: hex): The encoding to be used when generating the output

```javascript
import Database from 'better-sqlite3';
import bs3plus from 'better-sqlite3-plus';

const db = new Database('test.db');

bs3plus.crypto(db);

const { hash } = db
  .prepare(`select genPasswordHash(?, ?, ?, ?, ?, ?) as hash`)
  .get('password1!', 'randomSaltValue', 32, 500, 'sha512', 'hex');

const { hash } = db
  .prepare(`select genPasswordHash(?, ?, ?, ?, ?) as hash`)
  .get('password1!', 'randomSaltValue', 32, 500, 'sha512');

const { hash } = db
  .prepare(`select genPasswordHash(?, ?, ?, ?) as hash`)
  .get('password1!', 'randomSaltValue', 32, 500);

const { hash } = db
  .prepare(`select genPasswordHash(?, ?, ?) as hash`)
  .get('password1!', 'randomSaltValue', 32);

const { hash } = db
  .prepare(`select genPasswordHash(?, ?) as hash`)
  .get('password1!', 'randomSaltValue');
```

### genSalt

Generates a salt value based on the provided inputs

#### Parameters

- **size** (Default: 32): The size of the salt value in bytes
- **encoding** (Default: hex): The encoding to be used when generating the output

```javascript
import Database from 'better-sqlite3';
import bs3plus from 'better-sqlite3-plus';

const db = new Database('test.db');

bs3plus.crypto(db);

const { salt } = db.prepare(`select genSalt(?, ?) as salt`).get(32, 'hex');

const { salt } = db.prepare(`select genSalt(?) as salt`).get(32);

const { salt } = db.prepare(`select genSalt() as salt`).get();
```

## Statements

When adding the crypto functions module to your sqlite3 instance, a number of statements and
statement generators are also returned for you to use at your convenience.

### createSaltAndPasswordHash

This function receives as input an options object and returns a sqlite3 statment based on the
provided options. The options are:

- **algorithm** (Default: sha512): The algorithm to use when generating the has value
- **encoding** (Default: hex): The encoding to be used when generating the output
- **iterations** (Default: 500): Number of iterations to use when generating the hash
- **keyLength** (Default: 16): The length of the generated hash value

```javascript
import Database from 'better-sqlite3';
import bs3plus from 'better-sqlite3-plus';

const db = new Database('test.db');

const cryptoHelpers = bs3plus.crypto(db);

const myPasswordHashingStatement = cryptoHelpers.createSaltAndPasswordHash({
  algorithm: 'sha512',
  encoding: 'hex',
  iterations: 500,
  keyLength: 16
});

const result = myPasswordHashingStatement.get({ password: 'myPassword1!' });

console.log(result);

// PRINTS
// {
//   salt: '4ecc819ec883c5df7eac7615f80e0d23',
//   hash: '23c55169ff62921a018fbbd8616621d9'
// }
```
